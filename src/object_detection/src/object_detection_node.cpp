// Program collects points in point cloud and clusters them to a certain size.
// markerArray of found objects is published for rendering

#include <ros/ros.h>
#include <std_msgs/Int16.h>
#include <std_msgs/Bool.h>
#include <visualization_msgs/MarkerArray.h>
#include <pcl_ros/point_cloud.h>
#include <pcl_conversions/pcl_conversions.h>
#include <stdio.h>
#include <cmath>
#include <array>
#include <cstdint>
#include <random>
#include "object_detection/objects_msg.h"
#include "object_detection/PositionROS.h"
#include "Position.hpp"
#include "Settings.hpp"
#include "converters.hpp"
#include "Markers.hpp"
#include "ConvexHullJM.hpp"
#include "QuickHull.hpp"
#include <vector>

using namespace datatypes;
using namespace std;
using namespace quickhull;

// set to true if we only want points in the 180° in front of lidar
static constexpr bool JUST_FORWARD_POINTS = true;

//set proximity range (ignore objects closer than this)
static constexpr float IGNORE_OBJECTS_CLOSER_THAN_METERS = 1.5;

//declare pointcloud
typedef pcl::PointCloud<pcl::PointXYZ> PointCloud;

/**
 * Scaling factor for point cloud data
 */
static constexpr int POINT_SCALING_FACTOR = 1000; //was 1000

/**
 * Maximum number of rendered clusters. More objects = slower render but higher resolution
 */
static constexpr int INITIAL_CLUSTER_MAX_COUNT = 12; //was 20, was 50, was 7

/**
 * 40 points = 1cm 4000 points = 1m
 */
static constexpr int METERS_TO_POINTS_TRANSLATION = 4000;

// Global Settings instance
datatypes::Settings settings;

//publisher required in the callback function (thats why we use global variable)
ros::Publisher *pubObjects;
ros::Publisher *pubMarkerArray;
ros::Publisher *pubVessel;

void distanceMessageReceived(const std_msgs::Int16::ConstPtr &msg)
{
    int distance = msg->data;
    settings.setDetectionDistance(distance);
}

void degreeMessageReceived(const std_msgs::Bool::ConstPtr &msg)
{
    bool forward = msg->data;
    settings.setDetectionDistance(forward);
}

void clusterCenterMessageReceived(const std_msgs::Int16::ConstPtr &msg)
{
    int distance = msg->data;
    settings.setClusterCenterPointDistance(distance);
    ROS_INFO("center: %d", distance);
}

void minimumPointsMessageReceived(const std_msgs::Int16::ConstPtr &msg)
{
    int points = msg->data;
    settings.setMinimumPointsInCluster(points);
    ROS_INFO("minPoints: %d", points);
}

//* findObjects receives pointcloud data and groups the points into clusters//
std::vector<datatypes::Position> findObjects(const std::vector<datatypes::Position> &points)
{
    std::vector<datatypes::Position> clusters;
    int minPointsCluster = settings.getMinimumPointsInCluster();
    int clusterCenterDist = settings.getClusterCenterPointDistance();

    // check if there are actually any points in the desired detection Area
    //if (points.size() >= THRESHOLD_MIN_POINTS_IN_CLUSTER)
    if (points.size() >= minPointsCluster)
    {
        std::default_random_engine engine;
        std::uniform_int_distribution<int> uid(0, (points.size() - 1));
        std::uniform_real_distribution<double> urd(0, 1);
        int randomIndex = uid(engine);

        clusters.reserve(INITIAL_CLUSTER_MAX_COUNT);
        clusters.push_back(points[randomIndex]);

        for (int i = 1; i < INITIAL_CLUSTER_MAX_COUNT; i++)
        {
            int indexMaxValue = -1;
            double maxValue = 0;

            for (unsigned int i = 0; i < points.size(); i++)
            {
                // find distance to nearest cluster
                double minDist = 1e6;
                for (unsigned int c = 0; c < clusters.size(); c++)
                {
                    double dist = points[i].distanceTo(clusters[c]);
                    if ((dist * dist) < (minDist * minDist))
                    {
                        minDist = dist;
                    }
                }
                //if (minDist > THRESHOLD_DIST_TO_CLUSTER)
                if (minDist > clusterCenterDist)
                {
                    double value = urd(engine) * minDist * minDist;
                    if (value > maxValue)
                    {
                        indexMaxValue = i;
                        maxValue = value;
                    }
                }
            }
            if (indexMaxValue == -1)
            {
                break;
            }

            clusters.push_back(points[indexMaxValue]);
        }

        // Initial clusters found ==> now group
        std::vector<std::vector<datatypes::Position>> clusterPoints(clusters.size(), std::vector<datatypes::Position>());

        for (auto &&p : points)
        {
            // find nearest cluster
            double minDist = 1e6;
            int clusterIndex = -1;
            for (unsigned int c = 0; c < clusters.size(); c++)
            {
                double dist = p.distanceTo(clusters[c]);
                if ((dist * dist) < (minDist * minDist))
                {
                    minDist = dist;
                    clusterIndex = (int)c;
                }
            }
            if (clusterIndex != -1)
            {
                clusterPoints[clusterIndex].push_back(p);
            }
        }

        // recalculate centers for clusters
        clusters.clear();

        //message to send all object data points
        visualization_msgs::MarkerArray markerArray;

        //message to send all vessel object
        visualization_msgs::Marker vessel = addVesselMarker();
        pubVessel->publish(vessel);

        int id = 0; //revolving iterator to identify unique objects in each message
        for (unsigned int i = 0; i < clusterPoints.size(); i++)
        {

            //if (clusterPoints[i].size() < THRESHOLD_MIN_POINTS_IN_CLUSTER)
            if (clusterPoints[i].size() < minPointsCluster)
            {
                clusterPoints.erase(clusterPoints.begin() + i);
                i--;
            }
            else
            {
                QuickHull<float> qh;
                std::vector<Vector3<float>> pointcloud;
                std::vector<datatypes::Position> quickHullResult;
                object_detection::objects_msg quickHullMsg;

                std::vector<datatypes::Position> quickHullPoints;
                datatypes::Position quickHullPoint;

                int32_t x = 0, y = 0, z = 0, x_center = 0, y_center = 0, z_center = 0, totalPoints = 0;
                //*************** COULD POSSIBLY REMOVE X, Y AND /= THE XCENTRE YCENTRE VARS
                for (datatypes::Position p : clusterPoints[i])
                {
                    x += p.x;
                    y += p.y;
                    z += p.z;
                    totalPoints++;
                    Vector3<float> points;
                    points.x = p.x;
                    points.y = p.y;
                    points.z = p.z;
                    pointcloud.push_back(points);
                }

                x_center = x / totalPoints;
                y_center = y / totalPoints;
                z_center = z / totalPoints;

                clusters.push_back(datatypes::Position(x_center, y_center, 0));

                //objectPoints = (clusterPoints[i].x, clusterPoints[i].y, clusterPoints[i].z);

                auto hull = qh.getConvexHull(pointcloud, true, false);

                const auto &vertexBuffer = hull.getVertexBuffer();
                auto mesh = qh.getConvexHullAsMesh(&pointcloud[0].x, pointcloud.size(), true);

                for (auto it = vertexBuffer.begin(); it != vertexBuffer.end(); ++it)
                {
                    //cout << "vertex: " << *it << "\n";

                    quickHullPoint.x = it->x;
                    quickHullPoint.y = it->y;
                    quickHullPoint.z = it->z;
                    quickHullPoints.push_back(quickHullPoint);
                }
                

                //quickHullMsg.objects = datatypes::toROS<object_detection::PositionROS>(quickHullPoints);
                //pubObjects->publish(quickHullMsg);

                //pubObjects->publish(objectsList);

                //This variable will store the convex hull points from the known object cluster//
                std::vector<datatypes::Position> convexHullResult;
                int n = clusterPoints[i].size();
                convexHullResult = findConvexHull(clusterPoints[i], n);

                //markerArray.markers.push_back(addMarker(convexHullResult, 2, id++, datatypes::Position(x_center, y_center, z_center))); //create TRIANGLE_LIST marker
                markerArray.markers.push_back(addMarker(quickHullPoints, 2, id++, datatypes::Position(x_center, y_center, z_center))); //create TRIANGLE_LIST marker
            }
        }
     
        pubMarkerArray->publish(markerArray);
    }
    return clusters;
}

// A callback function. Executed each time a new pointcloud2 message arrives
void scannerMessageReceived(const PointCloud::ConstPtr &pPCL2)
{

    std::vector<datatypes::Position> filteredObjects;
    object_detection::objects_msg objectsList;
    std::vector<datatypes::Position> inDetectionField;

    // fetch current detection distance
    int max_x = settings.getDetectionDistance() * METERS_TO_POINTS_TRANSLATION;
    int max_y = settings.getDetectionDistance() * METERS_TO_POINTS_TRANSLATION;

    datatypes::Position transformed;

    int i = 0;
    bool forwardOnly = settings.getForwardOnly();
    //ROS_INFO("forward? %d", forwardOnly);

    BOOST_FOREACH (const pcl::PointXYZ &pt, pPCL2->points)
    {
        if (JUST_FORWARD_POINTS) //was const JUST_FORWARD_POINTS
        {
            if (pt.x < 0)
            {
                transformed.x = round(pt.x * POINT_SCALING_FACTOR);
                transformed.y = round(pt.y * POINT_SCALING_FACTOR);
                transformed.z = round(pt.z * POINT_SCALING_FACTOR);
            }
        }
        else
        {
            transformed.x = round(pt.x * POINT_SCALING_FACTOR);
            transformed.y = round(pt.y * POINT_SCALING_FACTOR);
            transformed.z = round(pt.z * POINT_SCALING_FACTOR);
        }

        //check if point is inside range of desired object detection
        if ((abs(transformed.x) < max_x) && (abs(transformed.y) < max_y))
        {
            //check if point is too close (closer than IGNORE_OBJECTS_CLOSER_THAN)
            float pointDistance = datatypes::Position(transformed.x, transformed.y, transformed.z).distanceTo(datatypes::Position(0, 0, 0));

            if (pointDistance >= (IGNORE_OBJECTS_CLOSER_THAN_METERS * METERS_TO_POINTS_TRANSLATION))
            {
                inDetectionField.push_back(transformed);
            }
        }
    }

    std::vector<datatypes::Position> objects = findObjects(inDetectionField);

    //objectsList.objects = datatypes::toROS<object_detection::PositionROS>(objects);

    //pubObjects->publish(objectsList);

    ///free up memory
    std::vector<datatypes::Position>().swap(inDetectionField);
    //std::vector<datatypes::Position>().swap(objects);
}

int main(int argc, char **argv)
{

    //Set up ROS
    ros::init(argc, argv, "object_detection");
    ros::NodeHandle nh;
    ROS_INFO("object detection node running");

    // Create a publisher object
    ros::Publisher objectPub = nh.advertise<object_detection::objects_msg>("objects", 1);
    pubObjects = &objectPub;

    ros::Publisher markerArrayPub = nh.advertise<visualization_msgs::MarkerArray>("marker_array", 5); // was 1000
    pubMarkerArray = &markerArrayPub;

    ros::Publisher vesselPub = nh.advertise<visualization_msgs::Marker>("vessel", 5);
    pubVessel = &vesselPub;

    // Create a scanner subscriber object
    ros::Subscriber sub = nh.subscribe("lslidar_point_cloud", 10, &scannerMessageReceived);

    ros::Subscriber distanceSub = nh.subscribe("distance", 10, &distanceMessageReceived);
    ros::Subscriber degreeeSub = nh.subscribe("degrees", 10, &degreeMessageReceived);
    ros::Subscriber clusterCenterSub = nh.subscribe("clusterCentre", 10, &clusterCenterMessageReceived);
    ros::Subscriber minClusterPointSub = nh.subscribe("clusterPoints", 10, &minimumPointsMessageReceived);

    ros::spin();
}
