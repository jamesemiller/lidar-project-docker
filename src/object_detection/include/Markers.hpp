#ifndef MARKERS_HPP
#define MARKERS_HPP

#include <ros/ros.h>
#include <visualization_msgs/Marker.h>
#include "Position.hpp"

using namespace datatypes;

visualization_msgs::Marker addVesselMarker();

visualization_msgs::Marker
addMarker(std::vector<datatypes::Position> result, int type, int id, datatypes::Position center);

#endif