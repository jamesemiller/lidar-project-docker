#ifndef CONVEXHULLJM_HPP
#define CONVEXHULLJM_HPP

#include <stack>
#include <algorithm>
#include <vector>
#include "Position.hpp"

using namespace datatypes;
using namespace std;

datatypes::Position secondTop(stack<datatypes::Position> &stk);
int squaredDist(datatypes::Position p1, datatypes::Position p2);
int direction(datatypes::Position a, datatypes::Position b, datatypes::Position c);
int comp(const void *point1, const void *point2);
vector<datatypes::Position> findConvexHull(vector<datatypes::Position> points, int n);


#endif