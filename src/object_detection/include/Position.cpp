/** ***************************************************************************
 *
 * @file
 * \addtogroup datatypes
 * @{
 ******************************************************************************
 */
#include "Position.hpp"
#include <iostream>
#include <fstream>
#include <cstdint>

using namespace std;

namespace datatypes
{

    /**
 * Constructs a position with x = 0 and y = 0
 */
    Position::Position() : x(0), y(0), z(0)
    {
    }

    /**
 * Constructs a position with x and y values.
 *
 * @param x the x value
 * @param y the y value
 * @param z the z value
 */
    Position::Position(int32_t x, int32_t y, int32_t z) : x(x), y(y), z(z)
    {
    }

    /**
 * Calculates the distance between this and other.
 *
 * @param other the other Position
 * @return the euclidian distance between this an other.
 */
    double Position::distanceTo(const Position &other) const
    {
        return getVectorTo(other).getLength();
    }

    /**
 * Convert Position to a Vector.
 *
 * @return the position vector of this position
 */
    Vector Position::asVectorFromOrigin() const
    {
        return Vector(x, y, z);
    }

    /**
 * Adding a vector moves the position appropriately.
 *
 * @param vector the vector describing the translation.
 * @return a Position that has been moved
 */
    Position Position::operator+(const Vector &vector) const
    {
        return (asVectorFromOrigin() + vector).asPosition();
    }

    /**
 * Subtracting a vector moves the position appropriately.
 *
 * @param vector the vector describing the translation.
 * @return a Position that has been moved
 */
    Position Position::operator-(const Vector &vector) const
    {
        return (asVectorFromOrigin() - vector).asPosition();
    }

    /**
 * Calculates the Vector from this Position to other.
 *
 * @param other the target Position
 * @return the Vector from this to other
 */
    Vector Position::getVectorTo(const Position &other) const
    {
        Vector result = other.asVectorFromOrigin() - asVectorFromOrigin();
        //printf("getVectorTo result: %f , %f\n", result.x, result.y);
        return (other.asVectorFromOrigin() - asVectorFromOrigin());
    }

    /**
 * Are the two Positions equal
 *
 * @param other the Position to compare to
 *
 * @retval true Positions are equal
 * @retval false Positions are different
 */
    bool Position::operator==(const Position &other) const
    {
        return (x == other.x) && (y == other.y) && (z == other.z);
    }

    /**
 * Are the two Positions different
 *
 * @param other the Position to compare to
 *
 * @retval true Positions are different
 * @retval false Positions are equal
 */
    bool Position::operator!=(const Position &other) const
    {
        return !(*this == other);
    }

    // function to wrote variable data to file
    int Position::toFile(const Position &other)
    {
        //file
        string filename = "/home/james/lidar/clusterData.csv";
        // Object to write in file
        ofstream file_obj;

        // Opening file in append mode
        //file_obj.open(filename.c_str(), std::ios::out | std::ios::binary);
        file_obj.open(filename.c_str(), std::ios::out | std::ios::binary);

        // Object of class position to input data in file
        Position obj;

        obj.x = other.x;
        obj.y = other.y;

        // Writing the object's data in file
        file_obj.write((char *)&obj, sizeof(obj));

        return 0;
    }

} // namespace datatypes

/** @} */
