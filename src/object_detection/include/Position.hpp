/** ***************************************************************************
 *
 * @file
 * \addtogroup datatypes
 * @{
 ******************************************************************************
 */

#ifndef POSITION_HPP
#define POSITION_HPP

#include "Vector.hpp"
#include <cstdint>

namespace datatypes {

// Forward declare Vector
class Vector;

/**
 * @brief A Position with X and Y coordinates
 *
 * @author  Moritz Hoewer (Moritz.Hoewer@haw-hamburg.de)
 * @version 2.0
 * @date    07.04.2019
 */
class Position {
public:
    int32_t x; ///< the x Position
    int32_t y; ///< the y Position
    int32_t z; ///< the z Position

public:
    Position();
    Position(int32_t x, int32_t y, int32_t z);
    virtual ~Position() = default;

    double distanceTo(const Position &other) const;
    Vector asVectorFromOrigin() const;

    Vector getVectorTo(const Position& other) const;
    Position operator-(const Vector& vector) const;
    Position operator+(const Vector& vector) const;

    bool operator==(const Position& other) const;
    bool operator!=(const Position& other) const;

    int toFile(const Position &other);
};

} // namespace datatypes
 #endif /* DATATYPES_POSITION_HPP */
/** @} */
