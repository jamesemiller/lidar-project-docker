

#ifndef SETTINGS_HPP
#define SETTINGS_HPP

#include <cstdint>

namespace datatypes

{
    class Settings
    {
    public:
        void setDetectionDistance(int dist);
        int getDetectionDistance();
        void setForwardOnly(bool forward);
        bool getForwardOnly();
        void setMinimumPointsInCluster(int points);
        int getMinimumPointsInCluster();
        void setClusterCenterPointDistance(int distance);
        int getClusterCenterPointDistance();


    private:
        int16_t detectionDistance = 50;
        bool forwardOnly = true;
        int16_t minimumPointsInCluster = 40;
        int16_t clusterCenterPointDistance = 200;
    };

}

// namespace datatypes
#endif /* DATATYPES_SETTINGS_HPP */
       /** @} */