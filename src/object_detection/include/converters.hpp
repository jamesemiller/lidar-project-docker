/** ***************************************************************************
 *
 * @file
 * \addtogroup datatypes
 * @{
 ******************************************************************************
 */
#ifndef DATATYPES_CONVERTERS_HPP
#define DATATYPES_CONVERTERS_HPP
// our datatypes
#include <Angle.hpp>
#include <Pose.hpp>
#include <Position.hpp>
#include <Vector.hpp>
#include "Angle.cpp"
#include "Vector.cpp"

// the generated ros datatypes
#include "object_detection/VectorROS.h"
#include "object_detection/PositionROS.h"
#include "object_detection/PoseROS.h"
#include "object_detection/AngleROS.h"

namespace datatypes {

// Convert Angle to ROS
inline object_detection::AngleROS toROS(const Angle& angle) {
  object_detection::AngleROS angleROS;
  angleROS.angle = angle.getAngleInRadian();
  return angleROS;
}

// Convert Angle from ROS
inline Angle fromROS(const object_detection::AngleROS& angle) {
  return Angle{angle.angle};
}

// Convert Position to ROS
inline object_detection::PositionROS toROS(const Position& position) {
  object_detection::PositionROS positionROS;
  positionROS.x = position.x;
  positionROS.y = position.y;
  positionROS.z = position.z;
  return positionROS;
}

// Convert Position from ROS
inline Position fromROS(const object_detection::PositionROS& position) {
  return Position{position.x, position.y, position.z};
}

// Convert Pose to ROS
inline object_detection::PoseROS toROS(const Pose& pose) {
  object_detection::PoseROS poseROS;
  poseROS.position = toROS(pose.position);
  poseROS.angle = toROS(pose.heading);
  return poseROS;
}

// Convert Pose from ROS
inline Pose fromROS(const object_detection::PoseROS& pose) {
  return Pose{fromROS(pose.position), fromROS(pose.angle)};
}

// Convert Vector to ROS
inline object_detection::VectorROS toROS(const Vector& vector) {
  object_detection::VectorROS vectorROS;
  vectorROS.x = vector.x;
  vectorROS.y = vector.y;
  return vectorROS;
}

// Convert Vector from ROS
inline Vector fromROS(const object_detection::VectorROS& vector) {
  return Vector{vector.x, vector.y};
}

// Convert generic std::vector to ROS
template<typename ROS_T, typename T>
inline std::vector<ROS_T> toROS(const std::vector<T>& vector){
  std::vector<ROS_T> vectorROS;
  vectorROS.reserve(vector.size());
  for(auto element : vector) {
    vectorROS.push_back(toROS(element));
  }
  return vectorROS;
}

// Convert generic std::vector from ROS
template<typename T, typename ROS_T>
inline std::vector<T> fromROS(const std::vector<ROS_T>& vectorROS){
  std::vector<T> vector;
  vector.reserve(vectorROS.size());
  for(auto element : vectorROS) {
    vector.push_back(fromROS(element));
  }
  return vector;
}

} // namespace datatypes

#endif /* DATATYPES_CONVERTERS_HP */
