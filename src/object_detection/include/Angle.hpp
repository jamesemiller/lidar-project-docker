/** ***************************************************************************
 *
 * @file
 * \addtogroup datatypes
 * @{
 ******************************************************************************
 */

#ifndef ANGLE_HPP
#define ANGLE_HPP

namespace datatypes {

/**
 * the number PI
 */
constexpr float PI = 3.1415926535f;

/**
 * @brief Represents an Angle
 *
 * Angle has range from 0 to just under 2 PI.
 *
 * @author  Moritz Hoewer (Moritz.Hoewer@haw-hamburg.de)
 * @version 2.0
 * @date 07.04.2019
 */
class Angle {
private:
    float radian; ///< the value of the angle in radians
public:
    Angle();
    Angle(float radians);
    virtual ~Angle() = default;

    static Angle getFromDegrees(float degrees);

    float getAngleInDegrees() const;
    float getAngleInDegreesAround(const Angle& center) const;
    float getAngleInDegreesAround(float degreesCenter) const;
    float getAngleInRadian() const;
    float getAngleInRadianAround(const Angle& center) const;
    float getAngleInRadianAround(float radianCenter) const;

    float sin() const;
    float cos() const;

    void add(const Angle& other);
    void subtract(const Angle& other);

    Angle& operator=(float radians);
    Angle operator-() const;

private:
    void setRadian(float radians);
    
    

};

Angle operator+(const Angle& left, const Angle& right);
Angle operator+(float left, const Angle& right);
Angle operator+(const Angle& left, float right);

Angle operator-(const Angle& left, const Angle& right);
Angle operator-(float left, const Angle& right);
Angle operator-(const Angle& left, float right);

namespace literals {
    /**
     * User defined literal for Angle in degrees
     *
     * @param deg the magnitude in degrees
     *
     * @return a corresponding Angle object
     */
    inline Angle operator ""_deg(long double deg) {
        return Angle::getFromDegrees(deg);
    }
    
    /**
     * User defined literal for Angle in degrees
     *
     * @param deg the magnitude in degrees
     *
     * @return a corresponding Angle object
     */
    inline Angle operator ""_deg(unsigned long long deg) {
        return Angle::getFromDegrees(deg);
    }
}

} // namespace datatypes

#endif /* DATATAYPES_ANGLE_HPP */
/** @} */
