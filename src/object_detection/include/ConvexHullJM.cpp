#include <stack>
#include <algorithm>
#include <vector>
#include "Position.hpp"
#include "ConvexHullJM.hpp"

using namespace datatypes;

datatypes::Position p0;

//*******************************************CONVEX HULLFUNCTIONS ********************************//
datatypes::Position secondTop(stack<datatypes::Position> &stk)
{
    datatypes::Position tempPoint = stk.top();
    stk.pop();
    datatypes::Position res = stk.top(); //get the second top element
    stk.push(tempPoint);                 //push previous top again
    return res;
}
int squaredDist(datatypes::Position p1, datatypes::Position p2)
{
    return ((p1.x - p2.x) * (p1.x - p2.x) + (p1.y - p2.y) * (p1.y - p2.y));
}
int direction(datatypes::Position a, datatypes::Position b, datatypes::Position c)
{
    int val = (b.y - a.y) * (c.x - b.x) - (b.x - a.x) * (c.y - b.y);
    if (val == 0)
        return 0; //colinear
    else if (val < 0)
        return 2; //anti-clockwise direction
    return 1;     //clockwise direction
}
int comp(const void *point1, const void *point2)
{
    datatypes::Position *p1 = (datatypes::Position *)point1;
    datatypes::Position *p2 = (datatypes::Position *)point2;
    int dir = direction(p0, *p1, *p2);
    if (dir == 0)
        return (squaredDist(p0, *p2) >= squaredDist(p0, *p1)) ? -1 : 1;
    return (dir == 2) ? -1 : 1;
}
vector<datatypes::Position> findConvexHull(vector<datatypes::Position> points, int n)
{
    vector<datatypes::Position> convexHullPoints;
    int minY = points[0].y, min = 0;
    for (int i = 1; i < n; i++)
    {
        int y = points[i].y;
        //find bottom most or left most point
        if ((y < minY) || (minY == y) && points[i].x < points[min].x)
        {
            minY = points[i].y;
            min = i;
        }
    }
    swap(points[0], points[min]); //swap min point to 0th location
    p0 = points[0];
    qsort(&points[1], n - 1, sizeof(datatypes::Position), comp); //sort points from 1 place to end
    int arrSize = 1;                                             //used to locate items in modified array
    for (int i = 1; i < n; i++)
    {
        //when the angle of ith and (i+1)th elements are same, remove points
        while (i < n - 1 && direction(p0, points[i], points[i + 1]) == 0)
            i++;
        points[arrSize] = points[i];
        arrSize++;
    }
    if (arrSize < 3)
        return convexHullPoints; //there must be at least 3 points, return empty list.
    //create a stack and add first three points in the stack
    stack<datatypes::Position> stk;
    stk.push(points[0]);
    stk.push(points[1]);
    stk.push(points[2]);
    for (int i = 3; i < arrSize; i++)
    { //for remaining vertices
        while (direction(secondTop(stk), stk.top(), points[i]) != 2)
            stk.pop(); //when top, second top and ith point are not making left turn, remove point
        stk.push(points[i]);
    }
    while (!stk.empty())
    {
        convexHullPoints.push_back(stk.top()); //add points from stack
        stk.pop();
    }
}
//********************************END CONVEX HULL FUNCTIONS************************************************//
