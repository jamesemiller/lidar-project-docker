

#include "Settings.hpp"
#include <cstdint>

namespace datatypes

{
    void Settings::setDetectionDistance(int dist)
    {
        detectionDistance = dist;
    }

    int Settings::getDetectionDistance()
    {
        return detectionDistance;
    }
    void Settings::setForwardOnly(bool forward)
    {
        forwardOnly = !forwardOnly;
    }

    bool Settings::getForwardOnly()
    {
        return forwardOnly;
    }

    void Settings::setMinimumPointsInCluster(int minPoints)
    {
        minimumPointsInCluster = minPoints;
    }

    int Settings::getMinimumPointsInCluster()
    {
        return minimumPointsInCluster;
    }

    void Settings::setClusterCenterPointDistance(int distance)
    {
        clusterCenterPointDistance = distance;
    }

    int Settings::getClusterCenterPointDistance()
    {
        return clusterCenterPointDistance;
    }

}
