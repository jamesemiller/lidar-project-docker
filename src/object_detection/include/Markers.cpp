

#include <ros/ros.h>
#include <visualization_msgs/Marker.h>
#include "Markers.hpp"
#include "Position.hpp"
#include <stdio.h>

using namespace datatypes;
using namespace std;

visualization_msgs::Marker addVesselMarker()
{
    visualization_msgs::Marker marker;
    // %Tag(MARKER_INIT)%
    marker.header.frame_id = "laser_link";
    marker.header.stamp = ros::Time::now();
    marker.ns = "object-detection";
    marker.type = visualization_msgs::Marker::CUBE;
    marker.action = visualization_msgs::Marker::ADD;
    marker.pose.orientation.w = 1.0;
    marker.id = 1;
    //marker.mesh_resource = "file:///home/james/model.dae";
    //marker.mesh_use_embedded_materials = true;
    // /home/james/Projects/Docker/src/object_detection/components/BFINAL.dae
    marker.pose.position.x = 0;
    marker.pose.position.y = 0;
    marker.pose.position.z = -10;
    marker.pose.orientation.x = 0.0;
    marker.pose.orientation.y = 0.0;
    marker.pose.orientation.z = 0.0;
    marker.pose.orientation.w = 1.0;
    marker.scale.x = 10;
    marker.scale.y = 30;
    marker.scale.z = 10;
    marker.color.a = 1.0; // Don't forget to set the alpha!
    marker.color.r = 0.0;
    marker.color.g = 1.0;
    marker.color.b = 0.0;

    marker.lifetime = ros::Duration(5.0);

    return (marker);
}

visualization_msgs::Marker
addMarker(std::vector<datatypes::Position> convexHullResult, int type, int id, datatypes::Position center)
{
    int maxZ = -1e6; //the highest vertical position of the object
    int minZ = 1e6;  //the lowest vertical position of the object
    int x, y, z;
    // ROS_INFO("NEW HULL");
    for (uint32_t i = 0; i < convexHullResult.size(); i++)
    {

        // x = convexHullResult[i].x * 0.001;
        // y = convexHullResult[i].y * 0.001;
        // z = convexHullResult[i].z * 0.001;
        // ROS_INFO("x:%d, y:%d, z:%d", x, y, z);
        if (convexHullResult[i].z * 0.001 > maxZ)
        {
            maxZ = convexHullResult[i].z * 0.001;
        }
        if (convexHullResult[i].z * 0.001 < minZ)
        {
            minZ = convexHullResult[i].z * 0.001;
        }
    }

    visualization_msgs::Marker marker;
    // %Tag(MARKER_INIT)%
    marker.header.frame_id = "laser_link";
    marker.header.stamp = ros::Time::now();
    marker.ns = "object_markers";
    marker.action = visualization_msgs::Marker::ADD;
    marker.pose.orientation.w = 1.0;
    marker.id = id;
    marker.lifetime = ros::Duration(0.5);

    switch (type)
    {
    case 0: //marker = point marker
    {
        marker.type = visualization_msgs::Marker::POINTS;
        marker.scale.x = 0.8;
        marker.scale.y = 0.5;
        // Points are green
        marker.color.g = 1.0f;
        marker.color.a = 1.0;

        for (uint32_t i = 0; i < convexHullResult.size(); ++i)
        {
            geometry_msgs::Point p;
            p.x = convexHullResult[i].x * 0.001;
            p.y = convexHullResult[i].y * 0.001;
            p.z = convexHullResult[i].z * 0.001; //was DETECTED_OBJ_RENDER_Z

            marker.points.push_back(p);
        };
        break;
    }

    case 1: //marker = line_strip
    {
        marker.type = visualization_msgs::Marker::LINE_STRIP;
        marker.scale.x = 0.5;
        //lines are red
        marker.color.r = 1.0;
        marker.color.a = 1.0;

        for (uint32_t i = 0; i < convexHullResult.size(); ++i)
        {
            geometry_msgs::Point p;
            p.x = convexHullResult[i].x * 0.001;
            p.y = convexHullResult[i].y * 0.001;
            p.z = convexHullResult[i].z * 0.001; //was DETECTED_OBJ_RENDER_Z

            marker.points.push_back(p);
        };
        //need to close the object shape, add first point again
        geometry_msgs::Point closeObject;
        closeObject.x = convexHullResult[0].x * 0.001;
        closeObject.y = convexHullResult[0].y * 0.001;
        closeObject.z = convexHullResult[0].z * 0.001; //was DETECTED_OBJ_RENDER_Z
        marker.points.push_back(closeObject);
        break;
    }

    case 2: //marker = triangle_list
    {
        marker.type = visualization_msgs::Marker::TRIANGLE_LIST;
        marker.scale.x = 1.0;
        marker.scale.y = 1.0;
        marker.scale.z = 1.0;
        //lines are red
        marker.color.r = 1.0;
        marker.color.a = 1.0;

        geometry_msgs::Point centrePoint;
        centrePoint.x = center.x * 0.001;
        centrePoint.y = center.y * 0.001;
        centrePoint.z = maxZ;
        geometry_msgs::Point p1;
        geometry_msgs::Point p2;
        std_msgs::ColorRGBA c;
        c.r = 1.0;
        c.g = 0.0;
        c.b = 0.0;
        c.a = 1.0;

        for (uint32_t i = 0; i < convexHullResult.size(); i++)
        {

            //last triangle
            if (i + 1 == convexHullResult.size())
            {
                p1.x = convexHullResult[i].x * 0.001;
                p1.y = convexHullResult[i].y * 0.001;
                p1.z = maxZ;
                marker.points.push_back(p1);
                marker.colors.push_back(c);
                p2.x = convexHullResult[0].x * 0.001;
                p2.y = convexHullResult[0].y * 0.001;
                p2.z = maxZ;
                marker.points.push_back(p2);
                marker.colors.push_back(c);
                marker.points.push_back(centrePoint);
                marker.colors.push_back(c);

                //vert. triangle 1
                //vertex 1
                marker.points.push_back(p1);
                marker.colors.push_back(c);
                //vertex 2
                //p1.z = minZ;
                p1.z = convexHullResult[i].z * 0.001;
                marker.points.push_back(p1);
                marker.colors.push_back(c);
                //vertex 3
                //p2.z = minZ;
                //p2.z = convexHullResult[i + 1].z * 0.001;
                marker.points.push_back(p2);
                marker.colors.push_back(c);

                //vert triangle 2
                //vertex 1
                marker.points.push_back(p2);
                marker.colors.push_back(c);
                //vertex 2
                //p2.z = maxZ;
                p2.z = convexHullResult[0].z * 0.001;
                marker.points.push_back(p2);
                marker.colors.push_back(c);
                //vertex 3
                //p1.z = maxZ;
                marker.points.push_back(p1);
                marker.colors.push_back(c);

                continue;
            }
            //successive triangles
            p1.x = convexHullResult[i].x * 0.001;
            p1.y = convexHullResult[i].y * 0.001;
            p1.z = maxZ;
            marker.points.push_back(p1);
            marker.colors.push_back(c);

            p2.x = convexHullResult[i + 1].x * 0.001;
            p2.y = convexHullResult[i + 1].y * 0.001;
            p2.z = maxZ;
            marker.points.push_back(p2);
            marker.colors.push_back(c);
            marker.points.push_back(centrePoint);
            marker.colors.push_back(c);

            //vert. triangle 1
            //vertex 1
            marker.points.push_back(p1);
            marker.colors.push_back(c);
            //vertex 2
            //p1.z = minZ;
            p1.z = convexHullResult[i].z * 0.001;
            marker.points.push_back(p1);
            marker.colors.push_back(c);
            //vertex 3
            //p2.z = minZ;
            //p2.z = convexHullResult[i + 1].z * 0.001;
            marker.points.push_back(p2);
            marker.colors.push_back(c);

            //vert triangle 2
            //vertex 1
            marker.points.push_back(p2);
            marker.colors.push_back(c);
            //vertex 2
            //p2.z = maxZ;
            p2.z = convexHullResult[i + 1].z * 0.001;
            marker.points.push_back(p2);
            marker.colors.push_back(c);
            //vertex 3
            //p1.z = maxZ;
            marker.points.push_back(p1);
            marker.colors.push_back(c);
        };
        break;
    }
    }

    return (marker);
}
