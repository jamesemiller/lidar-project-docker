/** ***************************************************************************
 * @file
 * \addtogroup datatypes
 * @{
 ******************************************************************************
 */

#ifndef DATATYPES_POSE_HPP
#define DATATYPES_POSE_HPP

#include "Position.hpp"
#include "Angle.hpp"

namespace datatypes {

/**
 * @brief A Pose with a position and a heading
 *
 * @author  Moritz Hoewer (Moritz.Hoewer@haw-hamburg.de)
 * @version 2.0
 * @date    07.04.2019
 */
struct Pose {
    Position position;
    Angle heading;

    Pose() :
            position(), heading() {
    }

    Pose(const Position& position, const Angle& heading) :
            position(position), heading(heading) {
    }
};

} // namespace datatypes
#endif /* DATATYPES_POSE_HPP */
/** @} */
