;; Auto-generated. Do not edit!


(when (boundp 'object_detection::VectorROS)
  (if (not (find-package "OBJECT_DETECTION"))
    (make-package "OBJECT_DETECTION"))
  (shadow 'VectorROS (find-package "OBJECT_DETECTION")))
(unless (find-package "OBJECT_DETECTION::VECTORROS")
  (make-package "OBJECT_DETECTION::VECTORROS"))

(in-package "ROS")
;;//! \htmlinclude VectorROS.msg.html


(defclass object_detection::VectorROS
  :super ros::object
  :slots (_x _y ))

(defmethod object_detection::VectorROS
  (:init
   (&key
    ((:x __x) 0.0)
    ((:y __y) 0.0)
    )
   (send-super :init)
   (setq _x (float __x))
   (setq _y (float __y))
   self)
  (:x
   (&optional __x)
   (if __x (setq _x __x)) _x)
  (:y
   (&optional __y)
   (if __y (setq _y __y)) _y)
  (:serialization-length
   ()
   (+
    ;; float32 _x
    4
    ;; float32 _y
    4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; float32 _x
       (sys::poke _x (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _y
       (sys::poke _y (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; float32 _x
     (setq _x (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _y
     (setq _y (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;;
   self)
  )

(setf (get object_detection::VectorROS :md5sum-) "ff8d7d66dd3e4b731ef14a45d38888b6")
(setf (get object_detection::VectorROS :datatype-) "object_detection/VectorROS")
(setf (get object_detection::VectorROS :definition-)
      "float32 x
float32 y

")



(provide :object_detection/VectorROS "ff8d7d66dd3e4b731ef14a45d38888b6")


