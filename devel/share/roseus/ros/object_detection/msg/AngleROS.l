;; Auto-generated. Do not edit!


(when (boundp 'object_detection::AngleROS)
  (if (not (find-package "OBJECT_DETECTION"))
    (make-package "OBJECT_DETECTION"))
  (shadow 'AngleROS (find-package "OBJECT_DETECTION")))
(unless (find-package "OBJECT_DETECTION::ANGLEROS")
  (make-package "OBJECT_DETECTION::ANGLEROS"))

(in-package "ROS")
;;//! \htmlinclude AngleROS.msg.html


(defclass object_detection::AngleROS
  :super ros::object
  :slots (_angle ))

(defmethod object_detection::AngleROS
  (:init
   (&key
    ((:angle __angle) 0.0)
    )
   (send-super :init)
   (setq _angle (float __angle))
   self)
  (:angle
   (&optional __angle)
   (if __angle (setq _angle __angle)) _angle)
  (:serialization-length
   ()
   (+
    ;; float32 _angle
    4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; float32 _angle
       (sys::poke _angle (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; float32 _angle
     (setq _angle (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;;
   self)
  )

(setf (get object_detection::AngleROS :md5sum-) "2d11dcdbe5a6f73dd324353dc52315ab")
(setf (get object_detection::AngleROS :datatype-) "object_detection/AngleROS")
(setf (get object_detection::AngleROS :definition-)
      "float32 angle

")



(provide :object_detection/AngleROS "2d11dcdbe5a6f73dd324353dc52315ab")


