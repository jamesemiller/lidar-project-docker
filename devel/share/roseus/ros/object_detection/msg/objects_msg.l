;; Auto-generated. Do not edit!


(when (boundp 'object_detection::objects_msg)
  (if (not (find-package "OBJECT_DETECTION"))
    (make-package "OBJECT_DETECTION"))
  (shadow 'objects_msg (find-package "OBJECT_DETECTION")))
(unless (find-package "OBJECT_DETECTION::OBJECTS_MSG")
  (make-package "OBJECT_DETECTION::OBJECTS_MSG"))

(in-package "ROS")
;;//! \htmlinclude objects_msg.msg.html


(defclass object_detection::objects_msg
  :super ros::object
  :slots (_objects ))

(defmethod object_detection::objects_msg
  (:init
   (&key
    ((:objects __objects) ())
    )
   (send-super :init)
   (setq _objects __objects)
   self)
  (:objects
   (&rest __objects)
   (if (keywordp (car __objects))
       (send* _objects __objects)
     (progn
       (if __objects (setq _objects (car __objects)))
       _objects)))
  (:serialization-length
   ()
   (+
    ;; object_detection/PositionROS[] _objects
    (apply #'+ (send-all _objects :serialization-length)) 4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; object_detection/PositionROS[] _objects
     (write-long (length _objects) s)
     (dolist (elem _objects)
       (send elem :serialize s)
       )
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; object_detection/PositionROS[] _objects
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _objects (let (r) (dotimes (i n) (push (instance object_detection::PositionROS :init) r)) r))
     (dolist (elem- _objects)
     (send elem- :deserialize buf ptr-) (incf ptr- (send elem- :serialization-length))
     ))
   ;;
   self)
  )

(setf (get object_detection::objects_msg :md5sum-) "3aa23f95847220504fca483ef2cdb8d3")
(setf (get object_detection::objects_msg :datatype-) "object_detection/objects_msg")
(setf (get object_detection::objects_msg :definition-)
      "#simple Point array x, y coords 

PositionROS[] objects
================================================================================
MSG: object_detection/PositionROS
int32 x
int32 y
int32 z

")



(provide :object_detection/objects_msg "3aa23f95847220504fca483ef2cdb8d3")


