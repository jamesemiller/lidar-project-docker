;; Auto-generated. Do not edit!


(when (boundp 'object_detection::PoseROS)
  (if (not (find-package "OBJECT_DETECTION"))
    (make-package "OBJECT_DETECTION"))
  (shadow 'PoseROS (find-package "OBJECT_DETECTION")))
(unless (find-package "OBJECT_DETECTION::POSEROS")
  (make-package "OBJECT_DETECTION::POSEROS"))

(in-package "ROS")
;;//! \htmlinclude PoseROS.msg.html


(defclass object_detection::PoseROS
  :super ros::object
  :slots (_position _angle ))

(defmethod object_detection::PoseROS
  (:init
   (&key
    ((:position __position) (instance object_detection::PositionROS :init))
    ((:angle __angle) (instance object_detection::AngleROS :init))
    )
   (send-super :init)
   (setq _position __position)
   (setq _angle __angle)
   self)
  (:position
   (&rest __position)
   (if (keywordp (car __position))
       (send* _position __position)
     (progn
       (if __position (setq _position (car __position)))
       _position)))
  (:angle
   (&rest __angle)
   (if (keywordp (car __angle))
       (send* _angle __angle)
     (progn
       (if __angle (setq _angle (car __angle)))
       _angle)))
  (:serialization-length
   ()
   (+
    ;; object_detection/PositionROS _position
    (send _position :serialization-length)
    ;; object_detection/AngleROS _angle
    (send _angle :serialization-length)
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; object_detection/PositionROS _position
       (send _position :serialize s)
     ;; object_detection/AngleROS _angle
       (send _angle :serialize s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; object_detection/PositionROS _position
     (send _position :deserialize buf ptr-) (incf ptr- (send _position :serialization-length))
   ;; object_detection/AngleROS _angle
     (send _angle :deserialize buf ptr-) (incf ptr- (send _angle :serialization-length))
   ;;
   self)
  )

(setf (get object_detection::PoseROS :md5sum-) "74a3daaeb1580f5ea10d5d89337033d1")
(setf (get object_detection::PoseROS :datatype-) "object_detection/PoseROS")
(setf (get object_detection::PoseROS :definition-)
      "PositionROS position
AngleROS angle

================================================================================
MSG: object_detection/PositionROS
int32 x
int32 y
int32 z

================================================================================
MSG: object_detection/AngleROS
float32 angle

")



(provide :object_detection/PoseROS "74a3daaeb1580f5ea10d5d89337033d1")


