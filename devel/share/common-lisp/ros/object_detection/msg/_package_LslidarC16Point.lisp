(cl:in-package object_detection-msg)
(cl:export '(TIME-VAL
          TIME
          X-VAL
          X
          Y-VAL
          Y
          Z-VAL
          Z
          AZIMUTH-VAL
          AZIMUTH
          DISTANCE-VAL
          DISTANCE
          INTENSITY-VAL
          INTENSITY
))