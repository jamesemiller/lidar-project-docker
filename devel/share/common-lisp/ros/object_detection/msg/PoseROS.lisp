; Auto-generated. Do not edit!


(cl:in-package object_detection-msg)


;//! \htmlinclude PoseROS.msg.html

(cl:defclass <PoseROS> (roslisp-msg-protocol:ros-message)
  ((position
    :reader position
    :initarg :position
    :type object_detection-msg:PositionROS
    :initform (cl:make-instance 'object_detection-msg:PositionROS))
   (angle
    :reader angle
    :initarg :angle
    :type object_detection-msg:AngleROS
    :initform (cl:make-instance 'object_detection-msg:AngleROS)))
)

(cl:defclass PoseROS (<PoseROS>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <PoseROS>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'PoseROS)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name object_detection-msg:<PoseROS> is deprecated: use object_detection-msg:PoseROS instead.")))

(cl:ensure-generic-function 'position-val :lambda-list '(m))
(cl:defmethod position-val ((m <PoseROS>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader object_detection-msg:position-val is deprecated.  Use object_detection-msg:position instead.")
  (position m))

(cl:ensure-generic-function 'angle-val :lambda-list '(m))
(cl:defmethod angle-val ((m <PoseROS>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader object_detection-msg:angle-val is deprecated.  Use object_detection-msg:angle instead.")
  (angle m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <PoseROS>) ostream)
  "Serializes a message object of type '<PoseROS>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'position) ostream)
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'angle) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <PoseROS>) istream)
  "Deserializes a message object of type '<PoseROS>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'position) istream)
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'angle) istream)
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<PoseROS>)))
  "Returns string type for a message object of type '<PoseROS>"
  "object_detection/PoseROS")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'PoseROS)))
  "Returns string type for a message object of type 'PoseROS"
  "object_detection/PoseROS")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<PoseROS>)))
  "Returns md5sum for a message object of type '<PoseROS>"
  "74a3daaeb1580f5ea10d5d89337033d1")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'PoseROS)))
  "Returns md5sum for a message object of type 'PoseROS"
  "74a3daaeb1580f5ea10d5d89337033d1")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<PoseROS>)))
  "Returns full string definition for message of type '<PoseROS>"
  (cl:format cl:nil "PositionROS position~%AngleROS angle~%~%================================================================================~%MSG: object_detection/PositionROS~%int32 x~%int32 y~%int32 z~%~%================================================================================~%MSG: object_detection/AngleROS~%float32 angle~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'PoseROS)))
  "Returns full string definition for message of type 'PoseROS"
  (cl:format cl:nil "PositionROS position~%AngleROS angle~%~%================================================================================~%MSG: object_detection/PositionROS~%int32 x~%int32 y~%int32 z~%~%================================================================================~%MSG: object_detection/AngleROS~%float32 angle~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <PoseROS>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'position))
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'angle))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <PoseROS>))
  "Converts a ROS message object to a list"
  (cl:list 'PoseROS
    (cl:cons ':position (position msg))
    (cl:cons ':angle (angle msg))
))
