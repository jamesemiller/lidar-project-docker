; Auto-generated. Do not edit!


(cl:in-package object_detection-msg)


;//! \htmlinclude AngleROS.msg.html

(cl:defclass <AngleROS> (roslisp-msg-protocol:ros-message)
  ((angle
    :reader angle
    :initarg :angle
    :type cl:float
    :initform 0.0))
)

(cl:defclass AngleROS (<AngleROS>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <AngleROS>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'AngleROS)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name object_detection-msg:<AngleROS> is deprecated: use object_detection-msg:AngleROS instead.")))

(cl:ensure-generic-function 'angle-val :lambda-list '(m))
(cl:defmethod angle-val ((m <AngleROS>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader object_detection-msg:angle-val is deprecated.  Use object_detection-msg:angle instead.")
  (angle m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <AngleROS>) ostream)
  "Serializes a message object of type '<AngleROS>"
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'angle))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <AngleROS>) istream)
  "Deserializes a message object of type '<AngleROS>"
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'angle) (roslisp-utils:decode-single-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<AngleROS>)))
  "Returns string type for a message object of type '<AngleROS>"
  "object_detection/AngleROS")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'AngleROS)))
  "Returns string type for a message object of type 'AngleROS"
  "object_detection/AngleROS")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<AngleROS>)))
  "Returns md5sum for a message object of type '<AngleROS>"
  "2d11dcdbe5a6f73dd324353dc52315ab")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'AngleROS)))
  "Returns md5sum for a message object of type 'AngleROS"
  "2d11dcdbe5a6f73dd324353dc52315ab")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<AngleROS>)))
  "Returns full string definition for message of type '<AngleROS>"
  (cl:format cl:nil "float32 angle~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'AngleROS)))
  "Returns full string definition for message of type 'AngleROS"
  (cl:format cl:nil "float32 angle~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <AngleROS>))
  (cl:+ 0
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <AngleROS>))
  "Converts a ROS message object to a list"
  (cl:list 'AngleROS
    (cl:cons ':angle (angle msg))
))
