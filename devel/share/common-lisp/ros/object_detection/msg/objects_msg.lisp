; Auto-generated. Do not edit!


(cl:in-package object_detection-msg)


;//! \htmlinclude objects_msg.msg.html

(cl:defclass <objects_msg> (roslisp-msg-protocol:ros-message)
  ((objects
    :reader objects
    :initarg :objects
    :type (cl:vector object_detection-msg:PositionROS)
   :initform (cl:make-array 0 :element-type 'object_detection-msg:PositionROS :initial-element (cl:make-instance 'object_detection-msg:PositionROS))))
)

(cl:defclass objects_msg (<objects_msg>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <objects_msg>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'objects_msg)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name object_detection-msg:<objects_msg> is deprecated: use object_detection-msg:objects_msg instead.")))

(cl:ensure-generic-function 'objects-val :lambda-list '(m))
(cl:defmethod objects-val ((m <objects_msg>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader object_detection-msg:objects-val is deprecated.  Use object_detection-msg:objects instead.")
  (objects m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <objects_msg>) ostream)
  "Serializes a message object of type '<objects_msg>"
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'objects))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (roslisp-msg-protocol:serialize ele ostream))
   (cl:slot-value msg 'objects))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <objects_msg>) istream)
  "Deserializes a message object of type '<objects_msg>"
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'objects) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'objects)))
    (cl:dotimes (i __ros_arr_len)
    (cl:setf (cl:aref vals i) (cl:make-instance 'object_detection-msg:PositionROS))
  (roslisp-msg-protocol:deserialize (cl:aref vals i) istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<objects_msg>)))
  "Returns string type for a message object of type '<objects_msg>"
  "object_detection/objects_msg")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'objects_msg)))
  "Returns string type for a message object of type 'objects_msg"
  "object_detection/objects_msg")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<objects_msg>)))
  "Returns md5sum for a message object of type '<objects_msg>"
  "3aa23f95847220504fca483ef2cdb8d3")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'objects_msg)))
  "Returns md5sum for a message object of type 'objects_msg"
  "3aa23f95847220504fca483ef2cdb8d3")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<objects_msg>)))
  "Returns full string definition for message of type '<objects_msg>"
  (cl:format cl:nil "#simple Point array x, y coords ~%~%PositionROS[] objects~%================================================================================~%MSG: object_detection/PositionROS~%int32 x~%int32 y~%int32 z~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'objects_msg)))
  "Returns full string definition for message of type 'objects_msg"
  (cl:format cl:nil "#simple Point array x, y coords ~%~%PositionROS[] objects~%================================================================================~%MSG: object_detection/PositionROS~%int32 x~%int32 y~%int32 z~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <objects_msg>))
  (cl:+ 0
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'objects) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ (roslisp-msg-protocol:serialization-length ele))))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <objects_msg>))
  "Converts a ROS message object to a list"
  (cl:list 'objects_msg
    (cl:cons ':objects (objects msg))
))
