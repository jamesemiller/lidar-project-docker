// Auto-generated. Do not edit!

// (in-package object_detection.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;
let PositionROS = require('./PositionROS.js');
let AngleROS = require('./AngleROS.js');

//-----------------------------------------------------------

class PoseROS {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.position = null;
      this.angle = null;
    }
    else {
      if (initObj.hasOwnProperty('position')) {
        this.position = initObj.position
      }
      else {
        this.position = new PositionROS();
      }
      if (initObj.hasOwnProperty('angle')) {
        this.angle = initObj.angle
      }
      else {
        this.angle = new AngleROS();
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type PoseROS
    // Serialize message field [position]
    bufferOffset = PositionROS.serialize(obj.position, buffer, bufferOffset);
    // Serialize message field [angle]
    bufferOffset = AngleROS.serialize(obj.angle, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type PoseROS
    let len;
    let data = new PoseROS(null);
    // Deserialize message field [position]
    data.position = PositionROS.deserialize(buffer, bufferOffset);
    // Deserialize message field [angle]
    data.angle = AngleROS.deserialize(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    return 16;
  }

  static datatype() {
    // Returns string type for a message object
    return 'object_detection/PoseROS';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '74a3daaeb1580f5ea10d5d89337033d1';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    PositionROS position
    AngleROS angle
    
    ================================================================================
    MSG: object_detection/PositionROS
    int32 x
    int32 y
    int32 z
    
    ================================================================================
    MSG: object_detection/AngleROS
    float32 angle
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new PoseROS(null);
    if (msg.position !== undefined) {
      resolved.position = PositionROS.Resolve(msg.position)
    }
    else {
      resolved.position = new PositionROS()
    }

    if (msg.angle !== undefined) {
      resolved.angle = AngleROS.Resolve(msg.angle)
    }
    else {
      resolved.angle = new AngleROS()
    }

    return resolved;
    }
};

module.exports = PoseROS;
