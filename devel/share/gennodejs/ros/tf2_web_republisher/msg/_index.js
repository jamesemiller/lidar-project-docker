
"use strict";

let TFSubscriptionAction = require('./TFSubscriptionAction.js');
let TFSubscriptionActionFeedback = require('./TFSubscriptionActionFeedback.js');
let TFSubscriptionActionResult = require('./TFSubscriptionActionResult.js');
let TFSubscriptionGoal = require('./TFSubscriptionGoal.js');
let TFSubscriptionResult = require('./TFSubscriptionResult.js');
let TFSubscriptionFeedback = require('./TFSubscriptionFeedback.js');
let TFSubscriptionActionGoal = require('./TFSubscriptionActionGoal.js');
let TFArray = require('./TFArray.js');

module.exports = {
  TFSubscriptionAction: TFSubscriptionAction,
  TFSubscriptionActionFeedback: TFSubscriptionActionFeedback,
  TFSubscriptionActionResult: TFSubscriptionActionResult,
  TFSubscriptionGoal: TFSubscriptionGoal,
  TFSubscriptionResult: TFSubscriptionResult,
  TFSubscriptionFeedback: TFSubscriptionFeedback,
  TFSubscriptionActionGoal: TFSubscriptionActionGoal,
  TFArray: TFArray,
};
