# This Python file uses the following encoding: utf-8
"""autogenerated by genpy from object_detection/PoseROS.msg. Do not edit."""
import codecs
import sys
python3 = True if sys.hexversion > 0x03000000 else False
import genpy
import struct

import object_detection.msg

class PoseROS(genpy.Message):
  _md5sum = "74a3daaeb1580f5ea10d5d89337033d1"
  _type = "object_detection/PoseROS"
  _has_header = False  # flag to mark the presence of a Header object
  _full_text = """PositionROS position
AngleROS angle

================================================================================
MSG: object_detection/PositionROS
int32 x
int32 y
int32 z

================================================================================
MSG: object_detection/AngleROS
float32 angle
"""
  __slots__ = ['position','angle']
  _slot_types = ['object_detection/PositionROS','object_detection/AngleROS']

  def __init__(self, *args, **kwds):
    """
    Constructor. Any message fields that are implicitly/explicitly
    set to None will be assigned a default value. The recommend
    use is keyword arguments as this is more robust to future message
    changes.  You cannot mix in-order arguments and keyword arguments.

    The available fields are:
       position,angle

    :param args: complete set of field values, in .msg order
    :param kwds: use keyword arguments corresponding to message field names
    to set specific fields.
    """
    if args or kwds:
      super(PoseROS, self).__init__(*args, **kwds)
      # message fields cannot be None, assign default values for those that are
      if self.position is None:
        self.position = object_detection.msg.PositionROS()
      if self.angle is None:
        self.angle = object_detection.msg.AngleROS()
    else:
      self.position = object_detection.msg.PositionROS()
      self.angle = object_detection.msg.AngleROS()

  def _get_types(self):
    """
    internal API method
    """
    return self._slot_types

  def serialize(self, buff):
    """
    serialize message into buffer
    :param buff: buffer, ``StringIO``
    """
    try:
      _x = self
      buff.write(_get_struct_3if().pack(_x.position.x, _x.position.y, _x.position.z, _x.angle.angle))
    except struct.error as se: self._check_types(struct.error("%s: '%s' when writing '%s'" % (type(se), str(se), str(locals().get('_x', self)))))
    except TypeError as te: self._check_types(ValueError("%s: '%s' when writing '%s'" % (type(te), str(te), str(locals().get('_x', self)))))

  def deserialize(self, str):
    """
    unpack serialized message in str into this message instance
    :param str: byte array of serialized message, ``str``
    """
    codecs.lookup_error("rosmsg").msg_type = self._type
    try:
      if self.position is None:
        self.position = object_detection.msg.PositionROS()
      if self.angle is None:
        self.angle = object_detection.msg.AngleROS()
      end = 0
      _x = self
      start = end
      end += 16
      (_x.position.x, _x.position.y, _x.position.z, _x.angle.angle,) = _get_struct_3if().unpack(str[start:end])
      return self
    except struct.error as e:
      raise genpy.DeserializationError(e)  # most likely buffer underfill


  def serialize_numpy(self, buff, numpy):
    """
    serialize message with numpy array types into buffer
    :param buff: buffer, ``StringIO``
    :param numpy: numpy python module
    """
    try:
      _x = self
      buff.write(_get_struct_3if().pack(_x.position.x, _x.position.y, _x.position.z, _x.angle.angle))
    except struct.error as se: self._check_types(struct.error("%s: '%s' when writing '%s'" % (type(se), str(se), str(locals().get('_x', self)))))
    except TypeError as te: self._check_types(ValueError("%s: '%s' when writing '%s'" % (type(te), str(te), str(locals().get('_x', self)))))

  def deserialize_numpy(self, str, numpy):
    """
    unpack serialized message in str into this message instance using numpy for array types
    :param str: byte array of serialized message, ``str``
    :param numpy: numpy python module
    """
    codecs.lookup_error("rosmsg").msg_type = self._type
    try:
      if self.position is None:
        self.position = object_detection.msg.PositionROS()
      if self.angle is None:
        self.angle = object_detection.msg.AngleROS()
      end = 0
      _x = self
      start = end
      end += 16
      (_x.position.x, _x.position.y, _x.position.z, _x.angle.angle,) = _get_struct_3if().unpack(str[start:end])
      return self
    except struct.error as e:
      raise genpy.DeserializationError(e)  # most likely buffer underfill

_struct_I = genpy.struct_I
def _get_struct_I():
    global _struct_I
    return _struct_I
_struct_3if = None
def _get_struct_3if():
    global _struct_3if
    if _struct_3if is None:
        _struct_3if = struct.Struct("<3if")
    return _struct_3if
