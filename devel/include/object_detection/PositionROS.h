// Generated by gencpp from file object_detection/PositionROS.msg
// DO NOT EDIT!


#ifndef OBJECT_DETECTION_MESSAGE_POSITIONROS_H
#define OBJECT_DETECTION_MESSAGE_POSITIONROS_H


#include <string>
#include <vector>
#include <map>

#include <ros/types.h>
#include <ros/serialization.h>
#include <ros/builtin_message_traits.h>
#include <ros/message_operations.h>


namespace object_detection
{
template <class ContainerAllocator>
struct PositionROS_
{
  typedef PositionROS_<ContainerAllocator> Type;

  PositionROS_()
    : x(0)
    , y(0)
    , z(0)  {
    }
  PositionROS_(const ContainerAllocator& _alloc)
    : x(0)
    , y(0)
    , z(0)  {
  (void)_alloc;
    }



   typedef int32_t _x_type;
  _x_type x;

   typedef int32_t _y_type;
  _y_type y;

   typedef int32_t _z_type;
  _z_type z;





  typedef boost::shared_ptr< ::object_detection::PositionROS_<ContainerAllocator> > Ptr;
  typedef boost::shared_ptr< ::object_detection::PositionROS_<ContainerAllocator> const> ConstPtr;

}; // struct PositionROS_

typedef ::object_detection::PositionROS_<std::allocator<void> > PositionROS;

typedef boost::shared_ptr< ::object_detection::PositionROS > PositionROSPtr;
typedef boost::shared_ptr< ::object_detection::PositionROS const> PositionROSConstPtr;

// constants requiring out of line definition



template<typename ContainerAllocator>
std::ostream& operator<<(std::ostream& s, const ::object_detection::PositionROS_<ContainerAllocator> & v)
{
ros::message_operations::Printer< ::object_detection::PositionROS_<ContainerAllocator> >::stream(s, "", v);
return s;
}


template<typename ContainerAllocator1, typename ContainerAllocator2>
bool operator==(const ::object_detection::PositionROS_<ContainerAllocator1> & lhs, const ::object_detection::PositionROS_<ContainerAllocator2> & rhs)
{
  return lhs.x == rhs.x &&
    lhs.y == rhs.y &&
    lhs.z == rhs.z;
}

template<typename ContainerAllocator1, typename ContainerAllocator2>
bool operator!=(const ::object_detection::PositionROS_<ContainerAllocator1> & lhs, const ::object_detection::PositionROS_<ContainerAllocator2> & rhs)
{
  return !(lhs == rhs);
}


} // namespace object_detection

namespace ros
{
namespace message_traits
{





template <class ContainerAllocator>
struct IsMessage< ::object_detection::PositionROS_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::object_detection::PositionROS_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct IsFixedSize< ::object_detection::PositionROS_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsFixedSize< ::object_detection::PositionROS_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct HasHeader< ::object_detection::PositionROS_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct HasHeader< ::object_detection::PositionROS_<ContainerAllocator> const>
  : FalseType
  { };


template<class ContainerAllocator>
struct MD5Sum< ::object_detection::PositionROS_<ContainerAllocator> >
{
  static const char* value()
  {
    return "3cb41a2c4416de195dbb95b7777a06fb";
  }

  static const char* value(const ::object_detection::PositionROS_<ContainerAllocator>&) { return value(); }
  static const uint64_t static_value1 = 0x3cb41a2c4416de19ULL;
  static const uint64_t static_value2 = 0x5dbb95b7777a06fbULL;
};

template<class ContainerAllocator>
struct DataType< ::object_detection::PositionROS_<ContainerAllocator> >
{
  static const char* value()
  {
    return "object_detection/PositionROS";
  }

  static const char* value(const ::object_detection::PositionROS_<ContainerAllocator>&) { return value(); }
};

template<class ContainerAllocator>
struct Definition< ::object_detection::PositionROS_<ContainerAllocator> >
{
  static const char* value()
  {
    return "int32 x\n"
"int32 y\n"
"int32 z\n"
;
  }

  static const char* value(const ::object_detection::PositionROS_<ContainerAllocator>&) { return value(); }
};

} // namespace message_traits
} // namespace ros

namespace ros
{
namespace serialization
{

  template<class ContainerAllocator> struct Serializer< ::object_detection::PositionROS_<ContainerAllocator> >
  {
    template<typename Stream, typename T> inline static void allInOne(Stream& stream, T m)
    {
      stream.next(m.x);
      stream.next(m.y);
      stream.next(m.z);
    }

    ROS_DECLARE_ALLINONE_SERIALIZER
  }; // struct PositionROS_

} // namespace serialization
} // namespace ros

namespace ros
{
namespace message_operations
{

template<class ContainerAllocator>
struct Printer< ::object_detection::PositionROS_<ContainerAllocator> >
{
  template<typename Stream> static void stream(Stream& s, const std::string& indent, const ::object_detection::PositionROS_<ContainerAllocator>& v)
  {
    s << indent << "x: ";
    Printer<int32_t>::stream(s, indent + "  ", v.x);
    s << indent << "y: ";
    Printer<int32_t>::stream(s, indent + "  ", v.y);
    s << indent << "z: ";
    Printer<int32_t>::stream(s, indent + "  ", v.z);
  }
};

} // namespace message_operations
} // namespace ros

#endif // OBJECT_DETECTION_MESSAGE_POSITIONROS_H
