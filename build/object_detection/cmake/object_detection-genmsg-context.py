# generated from genmsg/cmake/pkg-genmsg.context.in

messages_str = "/ros_workspace/src/lslidar_C16/lslidar_c16_msgs/msg/LslidarC16Layer.msg;/ros_workspace/src/lslidar_C16/lslidar_c16_msgs/msg/LslidarC16Packet.msg;/ros_workspace/src/lslidar_C16/lslidar_c16_msgs/msg/LslidarC16Point.msg;/ros_workspace/src/lslidar_C16/lslidar_c16_msgs/msg/LslidarC16Scan.msg;/ros_workspace/src/lslidar_C16/lslidar_c16_msgs/msg/LslidarC16Sweep.msg;/ros_workspace/src/lslidar_C16/lslidar_c16_msgs/msg/LslidarC16ScanUnified.msg;/ros_workspace/src/object_detection/msg/objects_msg.msg;/ros_workspace/src/object_detection/msg/AngleROS.msg;/ros_workspace/src/object_detection/msg/PoseROS.msg;/ros_workspace/src/object_detection/msg/PositionROS.msg;/ros_workspace/src/object_detection/msg/VectorROS.msg"
services_str = ""
pkg_name = "object_detection"
dependencies_str = "std_msgs;sensor_msgs;geometry_msgs;object_detection"
langs = "gencpp;geneus;genlisp;gennodejs;genpy"
dep_include_paths_str = "object_detection;/ros_workspace/src/lslidar_C16/lslidar_c16_msgs/msg;object_detection;/ros_workspace/src/object_detection/msg;std_msgs;/opt/ros/noetic/share/std_msgs/cmake/../msg;sensor_msgs;/opt/ros/noetic/share/sensor_msgs/cmake/../msg;geometry_msgs;/opt/ros/noetic/share/geometry_msgs/cmake/../msg;object_detection;/ros_workspace/src/lslidar_C16/lslidar_c16_msgs/msg;object_detection;/ros_workspace/src/object_detection/msg"
PYTHON_EXECUTABLE = "/usr/bin/python3"
package_has_static_sources = '' == 'TRUE'
genmsg_check_deps_script = "/opt/ros/noetic/share/genmsg/cmake/../../../lib/genmsg/genmsg_check_deps.py"
