# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "/ros_workspace/devel/include;/ros_workspace/src/object_detection/include;/ros_workspace/src/object_detection/msg".split(';') if "/ros_workspace/devel/include;/ros_workspace/src/object_detection/include;/ros_workspace/src/object_detection/msg" != "" else []
PROJECT_CATKIN_DEPENDS = "roscpp;lslidar_c16_msgs;pcl_ros;pcl_conversions".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "".split(';') if "" != "" else []
PROJECT_NAME = "object_detection"
PROJECT_SPACE_DIR = "/ros_workspace/devel"
PROJECT_VERSION = "1.0.0"
