# CMake generated Testfile for 
# Source directory: /ros_workspace/src
# Build directory: /ros_workspace/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("gtest")
subdirs("lslidar_C16/lslidar_c16")
subdirs("lslidar_C16/lslidar_c16_msgs")
subdirs("lslidar_C16/lslidar_c16_driver")
subdirs("lslidar_C16/lslidar_c16_decoder")
subdirs("object_detection")
subdirs("tf2_web_republisher")
