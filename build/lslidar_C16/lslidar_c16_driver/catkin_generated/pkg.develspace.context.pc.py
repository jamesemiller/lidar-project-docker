# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "/ros_workspace/src/lslidar_C16/lslidar_c16_driver/include".split(';') if "/ros_workspace/src/lslidar_C16/lslidar_c16_driver/include" != "" else []
PROJECT_CATKIN_DEPENDS = "roscpp;diagnostic_updater;nodelet;lslidar_c16_msgs".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "".split(';') if "" != "" else []
PROJECT_NAME = "lslidar_c16_driver"
PROJECT_SPACE_DIR = "/ros_workspace/devel"
PROJECT_VERSION = "1.2.0"
