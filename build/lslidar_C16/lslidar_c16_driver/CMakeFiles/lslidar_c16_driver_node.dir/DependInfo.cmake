# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/ros_workspace/src/lslidar_C16/lslidar_c16_driver/src/lslidar_c16_driver_node.cc" "/ros_workspace/build/lslidar_C16/lslidar_c16_driver/CMakeFiles/lslidar_c16_driver_node.dir/src/lslidar_c16_driver_node.cc.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "ROSCONSOLE_BACKEND_LOG4CXX"
  "ROS_BUILD_SHARED_LIBS=1"
  "ROS_PACKAGE_NAME=\"lslidar_c16_driver\""
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/ros_workspace/src/lslidar_C16/lslidar_c16_driver/include"
  "/ros_workspace/devel/include"
  "/opt/ros/noetic/include"
  "/opt/ros/noetic/share/xmlrpcpp/cmake/../../../include/xmlrpcpp"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/ros_workspace/build/lslidar_C16/lslidar_c16_driver/CMakeFiles/lslidar_c16_driver.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
