# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "/ros_workspace/devel/include".split(';') if "/ros_workspace/devel/include" != "" else []
PROJECT_CATKIN_DEPENDS = "tf;tf2_ros;geometry_msgs;actionlib;actionlib_msgs;message_runtime".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "".split(';') if "" != "" else []
PROJECT_NAME = "tf2_web_republisher"
PROJECT_SPACE_DIR = "/ros_workspace/devel"
PROJECT_VERSION = "0.3.2"
