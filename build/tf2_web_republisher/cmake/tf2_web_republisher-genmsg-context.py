# generated from genmsg/cmake/pkg-genmsg.context.in

messages_str = "/ros_workspace/devel/share/tf2_web_republisher/msg/TFSubscriptionAction.msg;/ros_workspace/devel/share/tf2_web_republisher/msg/TFSubscriptionActionGoal.msg;/ros_workspace/devel/share/tf2_web_republisher/msg/TFSubscriptionActionResult.msg;/ros_workspace/devel/share/tf2_web_republisher/msg/TFSubscriptionActionFeedback.msg;/ros_workspace/devel/share/tf2_web_republisher/msg/TFSubscriptionGoal.msg;/ros_workspace/devel/share/tf2_web_republisher/msg/TFSubscriptionResult.msg;/ros_workspace/devel/share/tf2_web_republisher/msg/TFSubscriptionFeedback.msg;/ros_workspace/src/tf2_web_republisher/msg/TFArray.msg"
services_str = "/ros_workspace/src/tf2_web_republisher/services/RepublishTFs.srv"
pkg_name = "tf2_web_republisher"
dependencies_str = "actionlib_msgs;std_msgs;geometry_msgs;roscpp"
langs = "gencpp;geneus;genlisp;gennodejs;genpy"
dep_include_paths_str = "tf2_web_republisher;/ros_workspace/devel/share/tf2_web_republisher/msg;tf2_web_republisher;/ros_workspace/src/tf2_web_republisher/msg;actionlib_msgs;/opt/ros/noetic/share/actionlib_msgs/cmake/../msg;std_msgs;/opt/ros/noetic/share/std_msgs/cmake/../msg;geometry_msgs;/opt/ros/noetic/share/geometry_msgs/cmake/../msg;roscpp;/opt/ros/noetic/share/roscpp/cmake/../msg"
PYTHON_EXECUTABLE = "/usr/bin/python3"
package_has_static_sources = '' == 'TRUE'
genmsg_check_deps_script = "/opt/ros/noetic/share/genmsg/cmake/../../../lib/genmsg/genmsg_check_deps.py"
