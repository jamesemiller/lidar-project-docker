# Object Detection using Lidar Point Clouds 

# Description
* The program connects to the leishen ls c16 lidar and will analyze the incoming point cloud data. Either 180° or 360° detection can be chosen. 
* Objects are determined by randomly selecting a known point in the pointcloud and then using vector calculus to determine the distance to related points. Points within the configured range are collected as a known cluster. A cluster ends when the distances to any other point in the area of points furthest from the initial point are out of the configured range. Cluster centers are calculated.
* The Cluster Array is sent through A Convex Hull algorithm and returns the outer-lying 3d points of the cluster.
* The Convex Hull result is sent to the Marker generation node. A ROS Message of type Marker_Array filled with Triangle_List Markers is generated. The maximum Z value is used for all points to develop a 'flat-top' polygon. The minimum Z values are retained relative to each point in the hull to emphasize the ploygonal rendering of potential overhead objects such as bridge arches.
# Usage
* **docker-compose run object_detection** starts the detection software without any lidar drivers (debug mode using rosbag)
* **docker-compose run lidar** starts the software with leishen lsc16 lidar drivers and looks to connect.
* **lidar IP:** 192.168.100.200
* **host IP:** 192.168.100.102



