FROM ros:noetic

# install build tools

RUN apt-get update && apt-get upgrade -y && apt-get install -y expect

COPY keyboard-configuration.exp /
RUN expect keyboard-configuration.exp

RUN apt-get install -y build-essential npm \
ros-noetic-pcl-ros npm iputils-ping ros-noetic-diagnostic-updater ros-noetic-rosbridge-suite

 #libssl1.1-dev nodejs-dev node-gyp npm
#libpcl-dev ros-kinetic-dynamic-reconfigure ros-melodic-nodelet ros-melodic-nodelet-topic-tools \
#ros-melodic-pluginlib \
#RUN apt-get install -y 
#RUN npm install rosnodejs



WORKDIR /ros_workspace
#RUN mkdir /src

#RUN /bin/bash -c '. /opt/ros/kinetic/setup.bash; cd /lidar/src; catkin_make'


#RUN apt-get install -y  libpcl-dev ros-melodic-dynamic-reconfigure ros-melodic-nodelet ros-melodic-nodelet-topic-tools \
#ros-melodic-pluginlib \
#nodejs-dev node-gyp libssl1.0-dev ros-melodic-pcl-ros build-essential iputils-ping npm ros-melodic-diagnostic-updater ros-melodic-rosbridge-suite
#RUN apt-get install -y 
